#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Fm Tx Usrp
# GNU Radio version: 3.8.1.0

from gnuradio import analog
from gnuradio import blocks
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time

class fm_tx_usrp(gr.top_block):

    def __init__(self, center_freq=3615e6, duration_s=2, nitems=1000000, tx_gain=35):
        gr.top_block.__init__(self, "Fm Tx Usrp")

        ##################################################
        # Parameters
        ##################################################
        self.center_freq = center_freq
        self.duration_s = duration_s
        self.nitems = nitems
        self.tx_gain = tx_gain

        ##################################################
        # Variables
        ##################################################
        self.tx_rate = tx_rate = 400e3
        self.nfilts = nfilts = 32
        self.bw = bw = 50000
        self.audio_rate = audio_rate = 40e3

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
            ",".join(('', '')),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
            '',
        )
        self.uhd_usrp_sink_0.set_center_freq(center_freq, 0)
        self.uhd_usrp_sink_0.set_gain(tx_gain, 0)
        self.uhd_usrp_sink_0.set_antenna('TX/RX', 0)
        self.uhd_usrp_sink_0.set_bandwidth(bw, 0)
        self.uhd_usrp_sink_0.set_samp_rate(tx_rate)
        self.uhd_usrp_sink_0.set_time_unknown_pps(uhd.time_spec())
        self.low_pass_filter_0 = filter.fir_filter_ccf(
            1,
            firdes.low_pass(
                1,
                audio_rate,
                5000,
                2000,
                firdes.WIN_HAMMING,
                6.76))
        self.blocks_head_0 = blocks.head(gr.sizeof_gr_complex*1, int(tx_rate*duration_s))
        self.blocks_add_xx_0 = blocks.add_vff(1)
        self.analog_sig_source_x_1 = analog.sig_source_f(audio_rate, analog.GR_COS_WAVE, 440, 0.1, 0, 0)
        self.analog_sig_source_x_0 = analog.sig_source_f(audio_rate, analog.GR_COS_WAVE, 280, 0.5, 0, 0)
        self.analog_nbfm_tx_0 = analog.nbfm_tx(
        	audio_rate=int(audio_rate),
        	quad_rate=int(tx_rate),
        	tau=75e-6,
        	max_dev=5e3,
        	fh=-1.0,
                )



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_nbfm_tx_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.analog_sig_source_x_1, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.blocks_add_xx_0, 0), (self.analog_nbfm_tx_0, 0))
        self.connect((self.blocks_head_0, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.blocks_head_0, 0))

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.uhd_usrp_sink_0.set_center_freq(self.center_freq, 0)

    def get_duration_s(self):
        return self.duration_s

    def set_duration_s(self, duration_s):
        self.duration_s = duration_s
        self.blocks_head_0.set_length(int(self.tx_rate*self.duration_s))

    def get_nitems(self):
        return self.nitems

    def set_nitems(self, nitems):
        self.nitems = nitems

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain
        self.uhd_usrp_sink_0.set_gain(self.tx_gain, 0)

    def get_tx_rate(self):
        return self.tx_rate

    def set_tx_rate(self, tx_rate):
        self.tx_rate = tx_rate
        self.blocks_head_0.set_length(int(self.tx_rate*self.duration_s))
        self.uhd_usrp_sink_0.set_samp_rate(self.tx_rate)

    def get_nfilts(self):
        return self.nfilts

    def set_nfilts(self, nfilts):
        self.nfilts = nfilts

    def get_bw(self):
        return self.bw

    def set_bw(self, bw):
        self.bw = bw
        self.uhd_usrp_sink_0.set_bandwidth(self.bw, 0)

    def get_audio_rate(self):
        return self.audio_rate

    def set_audio_rate(self, audio_rate):
        self.audio_rate = audio_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.audio_rate)
        self.analog_sig_source_x_1.set_sampling_freq(self.audio_rate)
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.audio_rate, 5000, 2000, firdes.WIN_HAMMING, 6.76))


def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-f", "--center-freq", dest="center_freq", type=eng_float, default="3.615G",
        help="Set center_freq [default=%(default)r]")
    parser.add_argument(
        "-d", "--duration-s", dest="duration_s", type=eng_float, default="2.0",
        help="Set duration_s [default=%(default)r]")
    parser.add_argument(
        "-N", "--nitems", dest="nitems", type=intx, default=1000000,
        help="Set Number of items to save [default=%(default)r]")
    parser.add_argument(
        "-g", "--tx-gain", dest="tx_gain", type=eng_float, default="35.0",
        help="Set tx_gain [default=%(default)r]")
    return parser


def main(top_block_cls=fm_tx_usrp, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(center_freq=options.center_freq, duration_s=options.duration_s, nitems=options.nitems, tx_gain=options.tx_gain)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
