#!/bin/bash

freq=914.1e6
gain=30

# for gain in 10 20 30 40 50 60 70 80 85 88
while true
do
  ./lora_jie_TX_hackrf.py -f $freq -g $gain &
  last_pid=$!
  sleep 5
  kill -9 $last_pid
  ./fm_tx_hackrf.py -f $freq -g $gain
  ./OFDM_TX_hackrf.py -f $freq -g $gain
done
